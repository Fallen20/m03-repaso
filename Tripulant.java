package exercici_reforc_de_1r;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class Tripulant{
	protected final String bandol = "Imperi Klingon";
	protected String ID;
	protected String nom;
	protected boolean actiu;
	protected LocalDateTime dataAlta;
	protected int departament;
	private int llocDeServei;
	
	public Tripulant(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int llocDeServei,int departament) {
		this.ID = iD;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.llocDeServei = llocDeServei;
		this.departament = departament;
	}
	
	
	public String getBandol() {return bandol;}
	public String getID() {return ID;}
	public String getNom() {return nom;}
	public boolean isActiu() {return actiu;}
	public LocalDateTime getDataAlta() {return dataAlta;}
	public int getLlocDeServei() {return llocDeServei;}
	public int getDepartament() {return departament;}

	//------------------------
	public void setID(String iD) {ID = iD;}
	public void setNom(String nom) {this.nom = nom;}
	public void setActiu(boolean actiu) {this.actiu = actiu;}
	public void setDataAlta(LocalDateTime dataAlta) {this.dataAlta = dataAlta;}
	public void setLlocDeServei(int llocDeServei) {this.llocDeServei = llocDeServei;}
	public void setDepartament(int departament) {this.departament = departament;}



	abstract String ImprimirDadesTripulant();//todas las clases lo heredan
	protected String saludar() {return "Hola des de la superclasse Tripulant";}






	@Override
	public int hashCode() {
		return Objects.hash(ID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		return Objects.equals(ID, other.ID);
	}






	@Override
	public String toString() {
		return "Tripulant [bandol=" + bandol + ", ID=" + ID + ", nom=" + nom + ", actiu=" + actiu + ", dataAlta="
				+ dataAlta + ", departament=" + departament + ", llocDeServei=" + llocDeServei + "]";
	}


	
}
