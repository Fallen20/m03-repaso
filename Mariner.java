package exercici_reforc_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Mariner extends Tripulant{

	private boolean serveiEnElPont;
	private String descripcioFeina;
	
	
	public Mariner(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int llocDeServei, int departament,
			boolean serveiEnElPont, String descripcioFeina) {
		super(iD, nom, actiu, dataAlta, llocDeServei, departament);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}
	//GETTER
	public boolean isServeiEnElPont() {return serveiEnElPont;}
	public String getDescripcioFeina() {return descripcioFeina;}


	//SETTER
	public void setDescripcioFeina(String descripcioFeina) {this.descripcioFeina = descripcioFeina;}
	public void setServeiEnElPont(boolean serveiEnElPont) {this.serveiEnElPont = serveiEnElPont;}


	protected String serveixEnElPont() {
		if(this.serveiEnElPont=true) {return "si";}
		else {return "no";}
	}
	
	

	@Override
	public String ImprimirDadesTripulant() {
		DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		//podemos ponerlo en la interface y asi solo se cambia
		//en una clase y no en todas que lo usen
		return "DADES TRIPULANT\n"+
				"\tBandol: "+super.bandol+"\n"
				+"\tID: "+super.ID+"\n"
				+"\tNom: "+super.nom+"\n"
				+"\tActiu: "+super.actiu+"\n"
				+"\tDeparament (de la clase Tripulant): "+super.departament+"\n"
				+"\tDepartament (de la clase IKSRotarranConstants): "+IKSRotarranConstants.departament[super.departament]+"\n"
				+"\tLloc de servei (de la clase Tripulant): "+super.getLlocDeServei()+"\n"
				+"\tLloc de servei (de la clase IKSRotarranConstants): "+IKSRotarranConstants.llocsDeServei[super.getLlocDeServei()]+"\n"
				+"\tDescripcio de la feina que fa:: "+this.descripcioFeina+"\n"
				+"\tServeix en el pont? "+serveixEnElPont()+"\n"
				+"\tData d'alta: "+super.dataAlta.format(fecha);
	}
	@Override
	public String toString() {
		return "Mariner [serveiEnElPont=" + serveiEnElPont + ", descripcioFeina=" + descripcioFeina + "]";
	}
	
	
	
	

}
