package exercici_reforc_de_1r;

import java.time.LocalDateTime;

public class Oficial extends Tripulant{

	private boolean serveiEnElPont;
	private String descripcioFeina;
	
	
	public Oficial(String iD, String nom, boolean actiu, LocalDateTime dataAlta, int llocDeServei, int departament,
			boolean serveiEnElPont, String descripcioFeina) {
		super(iD, nom, actiu, dataAlta, llocDeServei, departament);
		this.serveiEnElPont = serveiEnElPont;
		this.descripcioFeina = descripcioFeina;
	}
	//GETTER
	public boolean isServeiEnElPont() {return serveiEnElPont;}
	public String getDescripcioFeina() {return descripcioFeina;}


	//SETTER
	public void setDescripcioFeina(String descripcioFeina) {this.descripcioFeina = descripcioFeina;}
	public void setServeiEnElPont(boolean serveiEnElPont) {this.serveiEnElPont = serveiEnElPont;}


	private String serveixEnElPont() {
		if(this.serveiEnElPont=true) {return "si";}
		else {return "no";}
	}
	
	

	@Override
	public String ImprimirDadesTripulant() {
		return "Oficial"+
				"[serveiEnElPont=" + serveiEnElPont
				+", descripcioFeina=" + descripcioFeina 
				+", Tripulant [bandol="+ bandol 
				+", ID=" + super.ID 
				+", nom=" + super.nom 
				+", actiu=" + actiu
				+", dataAlta=" + dataAlta
				+ ", departament=" + departament
				+", llocDeServei="+super.getLlocDeServei()
				+"]]";
	}
	@Override
	protected String saludar() {
		return "Hola desde la subclase Oficial";
	}
	@Override
	public String toString() {
		return "Oficial [serveiEnElPont=" + serveiEnElPont + ", descripcioFeina=" + descripcioFeina + ", toString()="
				+ super.toString() + "]";
	}
	

	
	

}
