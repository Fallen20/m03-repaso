package exercici_reforc_de_1r;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class KSRotarran {

	public static void main(String[] args) {
		DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		Oficial capitan = new Oficial("001-A", "Martok", true, LocalDateTime.parse("15-08-1954 00:01", fecha), 1, 1, true, "Capitanejar la nau.");
		Mariner mariner_02_03=new Mariner("758-J","Kurak", true, LocalDateTime.parse("26-12-1981 13:42", fecha),1,3,true,"Mariner encarregat del timo i la navegacio durant el 2n torn");

		System.out.println("1- Se puede acceder al departament de capitan sin getter y setter?");
		System.out.println("Si, se puede acceder porque es protected y estamos accediendo desde la clase heredad");
		//se puede acceder porque es private y estamos accediendo desde la clase
		System.out.println("Departamento del capitan: "+capitan.departament);
		System.out.println();
		
		System.out.println("2- Se puede acceder a la descripcion de la feina de capitan sin getter y setter?");
		System.out.println("No, porque es private y no estamos accediendo desde la clase que lo tiene");
		System.out.println();
				
		System.out.println("3- Saca la descripcion de la feina del capitan");
		System.out.println("Con el getter,que es public");
		System.out.println(capitan.getDescripcioFeina());
		System.out.println();
		
		System.out.println("4- Saca el metodo ImprimirDadesTripulant");
		System.out.println(capitan.ImprimirDadesTripulant());
		//System.out.println(capitan.getNom()+" sirve en el pont? "+capitan.serveixEnElPont());
		System.out.println();
		
		System.out.println("5- Cambia el departamento del capitan al 10");
		System.out.println("Departamento del capitan antes: "+capitan.departament);
		capitan.departament=10;
		System.out.println("Departamento del capitan despues: "+capitan.departament);
		System.out.println();
		capitan.departament=1;
		
		//--------------------------------------
		Oficial oficialDeTipusOficial=new Oficial("001-A", "Marta", true, LocalDateTime.parse("15-08-1954 00:01", fecha), 1, 1, true, "Capitanejar la nau.");
		Tripulant oficialDeTipusTripulant=new Oficial("001-B", "Pepe", true, LocalDateTime.parse("15-08-1954 00:01", fecha), 1, 1, true, "Capitanejar la nau.");
		Oficial persona = new Oficial("001-H", "Martok", true, LocalDateTime.parse("15-08-1954 00:01", fecha), 1, 1, true, "Capitanejar la nau.");
		
		System.out.println("EJERCICIO 3");
		System.out.println("-Llama al metodo 'saludar' desde un objeto instanciado como Oficial y otro como Tripulant");
		System.out.println(oficialDeTipusOficial.saludar());
		System.out.println(oficialDeTipusTripulant.saludar());
		System.out.println();
		
		System.out.println("EJERCICIO 4");
		System.out.println("-Sobrescribe equals");
		System.out.println("ID oficialDeTipusOficial:"+oficialDeTipusOficial.ID+"\n"+
							"ID capitan:"+capitan.ID+"\n"+
							"ID persona:"+persona.ID);
		System.out.println("Son oficialDeTipusOficial y capitan la misma persona? "+oficialDeTipusOficial.equals(capitan));
		System.out.println("Son persona y capitan la misma persona? "+persona.equals(capitan));
		System.out.println();
		
		System.out.println("EJERCICIO 5");
		System.out.println("-Llama al lloc de servei con la interface");
		System.out.println("Lloc de servei: "+IKSRotarranConstants.llocsDeServei[capitan.getLlocDeServei()]);
		System.out.println("Departament: "+IKSRotarranConstants.departament[capitan.departament]);
		System.out.println();
		
		System.out.println("EJERCICIO 6");
		System.out.println("-Sobrescribe el toString de Tripulant, Mariner y Oficial. Haz que en Oficial se vea Tripulant");
		System.out.println("L'objecte capita (te implementat el toString()):\n " + capitan.toString());
		System.out.println();
		System.out.println("L'objecte mariner_02_03 (NO te implementat el toString()):n " + mariner_02_03.toString());
		System.out.println();
		
		System.out.println("EJERCICIO 7");
		System.out.println("-Sobrescribe imprimirDadesTripulant en Mariner");
		System.out.println(mariner_02_03.ImprimirDadesTripulant());
	}

}
